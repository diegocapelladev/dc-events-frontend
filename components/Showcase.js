
import styles from '../styles/Showcase.module.css'

export default function Showcase() {
  return (
    <div className={styles.showcase}>
      <h1>Bem Vindo a Festa!</h1>
      <h2>Encontre os eventos mais recentes</h2>
    </div>
  )
}