import { FaSignInAlt, FaSignOutAlt } from 'react-icons/fa'
import Link from 'next/link'

import AuthContext from '../context/AuthContext'
import styles from '../styles/Header.module.css'
import Search from './Search'
import { useContext } from 'react'

export default function Header() {
  const { user, logout } = useContext(AuthContext)

  return (
    <header className={styles.header}>
      <div className={styles.logo}>
        <Link href='/'>
          <a>DC Events</a>
        </Link>
      </div>

      <Search />

      <nav>
        <ul>
          <li>
            <Link href='/events'>
              <a>Eventos</a>
            </Link>
          </li>
          {
            user ? (
              <>
                <li>
                  <Link href='/events/add'>
                    <a>Adiconar Evento</a>
                  </Link>
                </li>
                <li>
                  <Link href='/account/dashboard'>
                    <a>Dashboard</a>
                  </Link>
                </li>
                <li>
                  <button 
                    onClick={() => logout()}
                    className='btn-secondary btn-icon'
                  >
                    <FaSignOutAlt /> Sair
                  </button>
                </li>
              </>
            ) : (
              <>
              <li>
                <Link href='/account/login'>
                  <a className='btn-secondary btn-icon'>
                    <FaSignInAlt /> Entrar
                  </a>
                </Link>
              </li>
              </>
            )
          }
        </ul>
      </nav>
    </header>
  )
}