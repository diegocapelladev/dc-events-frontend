
import Link from 'next/link'
import styles from '../styles/Footer.module.css'

export default function Footer() {
  return (
    <footer className={styles.footer}>
      <p>Copyright &copy; DC Events 2022</p>
      <p>
        <Link href='https://www.linkedin.com/in/diego-capella'>Diego Capella</Link>
      </p>
    </footer>
  )
}