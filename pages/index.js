import Link from "next/link";
import EvevntItem from "../components/EventItem";
import Layout from "../components/Layout";
import { API_URL } from "../config";


export default function HomePage({ events }) {
  return (
    <Layout>
      <h1>Próximos Eventos</h1>

      {
        events.length === 0 && <h3>Nenhum evento para mostrar</h3>
      }

      {
        events.map(evt => (
          <EvevntItem key={evt.id} evt={evt} />
        ))
      }

      {
        events.length > 0 && (
          <Link href='/events'>
            <a className='btn-secondary'>Ver todos os eventos</a>
          </Link>
        )
      }
    </Layout>
  )
}


export async function getServerSideProps() {
  const res = await fetch(`${API_URL}/events?_sort=date:ASC&_limit=5`)
  const events = await res.json()

  return {
    props: {
      events
    }
  }
}