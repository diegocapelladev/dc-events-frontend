import Image from "next/image";
import Link from "next/link";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css'
import Layout from "../../components/Layout";
import { API_URL } from "../../config";
import styles from '../../styles/Event.module.css'

export default function EventPage({ evt }) {
  return (
    <Layout>
      <div className={styles.event}>
        <span>
          {new Date(evt.date).toLocaleDateString('pt-BR', { day: '2-digit', month: 'long', year: 'numeric' })} às {evt.time}
        </span>
        <h1>{evt.name}</h1>
        <ToastContainer />
        {
          evt.image && (
            <div className={styles.image}>
              <Image src={evt.image.formats?.medium?.url === undefined ? evt.image.formats.small.url : evt.image.formats.medium.url} alt='' width={960} height={600} />
            </div>
          )
        }

        <h3>Artistas:</h3>
        <p>{evt.performers}</p>
        <h3>Descrição:</h3>
        <p>{evt.description}</p>
        <h3>Local: {evt.venue}</h3>
        <p>{evt.address}</p>

        <Link href='/events'>
          <a className={styles.back}>{'<'} Voltar</a>
        </Link>
      </div>
    </Layout>
  )
}

export async function getServerSideProps({ query: { slug } }) {
  const res = await fetch(`${API_URL}/events?slug=${slug}`)
  const events = await res.json()

  return {
    props: {
      evt: events[0],
    },
  }
}