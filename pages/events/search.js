import qs from 'qs'
import Link from "next/link"
import { useRouter } from "next/router"

import EvevntItem from "../../components/EventItem"
import Layout from "../../components/Layout"
import { API_URL } from "../../config"

export default function SearchPage({ events }) {
  const router = useRouter()

  return (
    <div>
      <Layout title='Resultados da busca'>
      <Link href='/events'>Voltar</Link>
      <h1>Resultados da busca por {router.query.term}</h1>

      {
        events.length === 0 && <h3>Nenhum evento para mostrar</h3>
      }

      {
        events.map(evt => (
          <EvevntItem key={evt.id} evt={evt} />
        ))
      }
    </Layout>
    </div>
  )
}

export async function getServerSideProps({ query: { term } }) {
  const query = qs.stringify({
    _where: {
      _or: [
        { name_contains: term },
        { performers_contains: term },
        { description_contains: term },
        { venue_contains: term },
      ],
    },
  })

  const res = await fetch(`${API_URL}/events?${query}`)
  const events = await res.json()

  return {
    props: { events },
  }
}