import Link from "next/link"
import { FaExclamationTriangle } from 'react-icons/fa'

import Layout from "../components/Layout"
import styles from '../styles/404.module.css'

export default function NotFoundPage() {
  return (
    <Layout title='Page Not Found'>
      <div className={styles.error}>
        <h1> <FaExclamationTriangle /> 404</h1>
        <h4>Desculpe, não há nada aqui...</h4>
        <Link href='/'>Voltar para a Home</Link>
      </div>
    </Layout>
  )
}