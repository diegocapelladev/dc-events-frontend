import Link from 'next/link'
import { useContext, useEffect, useState } from 'react'
import { FaUser } from 'react-icons/fa'
import { toast, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import Layout from '../../components/Layout'
import AuthContext from '../../context/AuthContext'
import styles from '../../styles/AuthForm.module.css'

export default function RegisterPage() {
  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [passwordConfirm, setPasswordConfirm] = useState('')

  const { register, error } = useContext(AuthContext)

  useEffect(() => error && toast.error(error))

  const handleSubmit = (e) => {
    e.preventDefault()

    if (password !== passwordConfirm) {
      toast.error('Senhas devem ser iguais!')
      return
    }

    register({ username, email, password })
  }

  return (
    <Layout title='Cadastrar'>
      <div className={styles.auth}>
        <h1>
          <FaUser /> Cadastrar
        </h1>
        <ToastContainer />
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor='username'>Nome</label>
            <input 
              type='text' 
              id='username'
              placeholder='nome' 
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor='email'>Email</label>
            <input 
              type='email' 
              id='email'
              placeholder='email' 
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor='password'>Senha</label>
            <input 
              type='password' 
              id='password'
              placeholder='senha' 
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor='passwordConfirm'>Confirme a Senha</label>
            <input 
              type='password' 
              id='passwordConfirm'
              placeholder='senha' 
              value={passwordConfirm}
              onChange={(e) => setPasswordConfirm(e.target.value)}
            />
          </div>
          <input type='submit' value='Cadastar' className='btn' />
        </form>

        <p>
          Já tem uma conta? <Link href='/account/login'>Login</Link>
        </p>
      </div>
    </Layout>
  )
}